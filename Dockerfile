FROM ccr.ccs.tencentyun.com/jerry/test:nginxV1
MAINTAINER lijx lijx0924@aliyun.com
RUN mkdir -p /data/www/wordpress
ADD . /data/www/wordpress
EXPOSE 80
ENTRYPOINT ["/usr/sbin/nginx"]